<?php

namespace Drupal\commerce_reports\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\Date;

/**
 * Custom Filter to handle end dates stored as a timestamp.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("search")
 */
class Search extends Date {

  /**
   * {@inheritdoc}
   */
  public function buildExposedForm(&$form, FormStateInterface $form_state): void {
    parent::buildExposedForm($form, $form_state);
    if ($this->operator === 'Custom Date') {
      $form['search']['min'] = [
        '#type' => 'date',
        '#title' => $this->t('Start Date'),
        '#default_value' => $this->options['search']['min'],
      ];
      $form['search']['max'] = [
        '#type' => 'date',
        '#title' => $this->t('End Date'),
        '#default_value' => $this->options['search']['max'],
      ];
    }
  }

  /**
   * Returns an array of operator information.
   */
  public function operators(): array {
    $operators = parent::operators();
    $operators += [

      'today' => [
        'title' => $this->t('Today'),
        'short' => $this->t('today'),
        'method' => 'today',
        'values' => 0,
      ],

      'yesterday' => [
        'title' => $this->t('Yesterday'),
        'short' => $this->t('yest'),
        'method' => 'opDayOfWeek',
        'values' => 0,
      ],

      'Last 30 days' => [
        'title' => $this->t('Last 30 days'),
        'short' => $this->t('month'),
        'method' => 'opDayOfMonth',
        'values' => 0,
      ],

      'Custom Date' => [
        'title' => $this->t('Custom Date'),
        'short' => $this->t('Date'),
        'method' => 'customDate',
        'values' => 2,
      ],
    ];
    return $operators;
  }

  /**
   * Adds a WHERE clause to the query.
   */
  protected function customDate($field): void {

    $min = str_replace('/', '-', $this->value['min']);
    $max = str_replace('/', '-', $this->value['max']);
    $this->value['min'] = strtotime($min);
    // If End date is current date.
    if (strtotime($max) == strtotime(date('Y-m-d'))) {
      $this->value['max'] = strtotime('now');
    }
    // If End date is a past date.
    if (strtotime($max) < strtotime(date('Y-m-d'))) {
      // Increment the date by one.
      $this->value['max'] = strtotime('+1 day', strtotime($max));
    }
    // If End Date is a future date.
    if (strtotime($max) > strtotime(date('Y-m-d'))) {
      $this->value['max'] = strtotime($max);
    }
    $this->query->addWhere($this->options['group'], $field,
      [$this->value['min'], $this->value['max']], 'BETWEEN');
  }

  /**
   * Adds a WHERE clause to the query.
   */
  protected function opDayOfWeek($field): void {
    $current_time = strtotime(date('d-m-Y'));
    $yesterday = strtotime('-1 day', strtotime(date('d-m-Y')));

    $this->query->addWhere($this->options['group'], $field,
      [$yesterday, $current_time], 'BETWEEN');
  }

  /**
   * Adds a WHERE clause to the query.
   */
  protected function opDayOfMonth($field): void {
    $current_time = strtotime('now');
    $last_month = strtotime('-30 day', strtotime(date('d-m-Y')));

    $this->query->addWhere($this->options['group'],
      $field, [$last_month, $current_time], 'BETWEEN');
  }

  /**
   * Adds a WHERE clause to the query.
   */
  protected function today($field): void {
    $today = strtotime(date('d-m-Y'));
    $current_time = strtotime('now');
    $this->query->addWhere($this->options['group'], $field,
      [$today, $current_time], 'BETWEEN');
  }

}
